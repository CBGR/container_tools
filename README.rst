Container Tools
===============

This repository contains a number of tools for manipulating container images
and software extracted from containers as simple packages. For the most part,
these tools just invoke ``buildah`` and ``docker``, but they also perform
tasks such as copying software into environments and invoking deployment
scripts.

Getting Started
---------------

These tools can be installed by running the ``deploy.sh`` script. For general
use, the default ``container`` profile can be used:

.. code:: shell

  ./deploy.sh

For use when deploying applications in a typical system-level environment, the
``web`` profile should also be used:

.. code:: shell

  ./deploy.sh -p web

The ``sudo`` command must be available to the user installing these tools.

Copyright and Licensing Information
-----------------------------------

Source code originating in this project is licensed under the terms of the GNU
General Public License version 3 or later (GPLv3 or later). Original content
is licensed under the Creative Commons Attribution Share Alike 4.0
International (CC-BY-SA 4.0) licensing terms.

See the ``.reuse/dep5`` file and the accompanying ``LICENSES`` directory in
this software's source code distribution for complete copyright and licensing
information, including the licensing details of bundled code and content.
