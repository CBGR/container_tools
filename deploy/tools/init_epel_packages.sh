#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`

. "$THISDIR/system.sh"

# Test for the EPEL and PowerTools package channel details.

if rpm -q epel-release && rpm -q dnf-plugins-core && redhat_have_repository 'powertools' ; then
    exit 0
fi

# Handle different distributions.

FLAVOUR=`redhat_flavour`

if [ "$FLAVOUR" = 'CentOS' ] ; then
    VERSION=`redhat_major_version`
    dnf install -y "https://dl.fedoraproject.org/pub/epel/epel-release-latest-$VERSION.noarch.rpm"

elif [ "$FLAVOUR" = 'AlmaLinux' ] ; then
    dnf install -y epel-release
fi

# Enable additional repositories.

dnf install -y dnf-plugins-core
dnf config-manager --set-enabled powertools

# vim: tabstop=4 expandtab shiftwidth=4
