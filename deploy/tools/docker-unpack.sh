#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

PROGNAME=$(basename "$0")
THISDIR=$(dirname "$0")
TOOLS=$(realpath "$THISDIR")

CONTAINER=$1
SOURCE=$2
TARGET=$3

if [ ! "$CONTAINER" ] || [ ! -e "$SOURCE" ] || [ ! "$TARGET" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <container> <source> <target>

Copy the source archive into the container and unpack it into the target
location.
EOF
    exit 1
fi

# Make a temporary location for the archive.

COPIED_SOURCE="/tmp/"`basename "$SOURCE"`

# Copy this repository's tools into the container as well as the archive.

docker cp "$TOOLS" "$CONTAINER:/tmp/tools"
docker cp "$SOURCE" "$CONTAINER:$COPIED_SOURCE"

# Run the unpack tool to populate the target location. Then delete the copied
# archive and the tools.

docker exec "$CONTAINER" "/tmp/tools/unpack.sh" "$COPIED_SOURCE" "$TARGET"
docker exec "$CONTAINER" rm "$COPIED_SOURCE"
docker exec "$CONTAINER" rm -r "/tmp/tools"

# vim: tabstop=4 expandtab shiftwidth=4
