#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=$(dirname "$0")

SOURCE=$1
TARGET=$2

"$THISDIR/tar_extract" "$SOURCE" "$TARGET"

# vim: tabstop=4 expandtab shiftwidth=4
