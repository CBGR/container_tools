#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/../../.."`

. "$BASEDIR/deploy/tools/system.sh"

# Allow Apache-resident application to be able to send mail.
# See: https://bugzilla.redhat.com/show_bug.cgi?id=538178

if have setsebool ; then
    setsebool -P httpd_can_sendmail on
fi

# vim: tabstop=4 expandtab shiftwidth=4
