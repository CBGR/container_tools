#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/../../.."`

. "$BASEDIR/deploy/tools/system.sh"

# Label the applications directory and its contents.

if have semanage ; then
    semanage fcontext -a -t httpd_sys_content_t "/var/www/apps(/.*)?"
    restorecon -R /var/www/apps
fi

# vim: tabstop=4 expandtab shiftwidth=4
