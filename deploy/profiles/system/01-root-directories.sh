#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Make the deployment directory.

mkdir -p /var/www/apps
chgrp webapps /var/www/apps
chmod g+rwx /var/www/apps

# Propagate group access to new directories.

chmod g+s /var/www/apps

# vim: tabstop=4 expandtab shiftwidth=4
