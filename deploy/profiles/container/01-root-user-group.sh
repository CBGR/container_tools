#!/bin/sh

# SPDX-FileCopyrightText: 2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/../../..")

# Obtain the unprivileged user details and add them to the group.

USERNAME=$(cat "$BASEDIR/deploy/work/username.txt")

usermod -G webapps -a "$USERNAME"

# vim: tabstop=4 expandtab shiftwidth=4
