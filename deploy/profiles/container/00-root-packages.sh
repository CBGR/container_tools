#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/../../.."`

. "$BASEDIR/deploy/tools/system.sh"

# Install system dependencies.

if have dnf ; then

    # Ensure that various additional software sources are available.

    "$BASEDIR/deploy/tools/init_epel_packages.sh"
    xargs dnf -y install < "$BASEDIR/requirements/requirements-sys-container.txt"

elif have apt ; then
    DEBIAN_FRONTEND=noninteractive xargs apt -y install < "$BASEDIR/requirements/requirements-sys-container-debian.txt"
else
    echo "System type not recognised." 1>&2
    exit 1
fi

# vim: tabstop=4 expandtab shiftwidth=4
