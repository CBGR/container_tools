#!/bin/sh

# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/../../.."`
DEPLOY="$BASEDIR/deploy"

# Record the unprivileged user running these scripts so that they may gain
# access to the appropriate group.

if [ ! -e "$DEPLOY/work" ] ; then
    mkdir -p "$DEPLOY/work"
fi

echo "$USER" > "$DEPLOY/work/username.txt"

# vim: tabstop=4 expandtab shiftwidth=4
