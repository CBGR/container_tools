#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Make a local group for deployment.

groupadd webapps || true

# vim: tabstop=4 expandtab shiftwidth=4
