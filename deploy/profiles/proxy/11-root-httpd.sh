#!/bin/sh

# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/../../.."`

# Obtain any site identifier and use that instead of the default.

SITE=`cat "$BASEDIR/deploy/work/site.txt"`

"$BASEDIR/deploy/tools/init_site" "$SITE"

# vim: tabstop=4 expandtab shiftwidth=4
