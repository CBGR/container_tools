#!/bin/sh

# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/../../.."`
DEPLOY="$BASEDIR/deploy"

# Obtain any data location and site identifier and use the indicated values
# instead of the default.

SITE=
SITE_DEFAULT="test-jaspar.uio.no"

# Proxy-related customisations.

PORT=
PORT_DEFAULT="8080"
SITE_PROFILE=
SITE_PROFILE_DEFAULT="site"
SECURE=

while getopts p:P:s:S NAME ; do
    case "$NAME" in
        p) PORT=$OPTARG ;;
        P) SITE_PROFILE=$OPTARG ;;
        s) SITE=$OPTARG ;;
        S) SECURE=1 ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

# Record the site details for other operations.

if [ ! -e "$DEPLOY/work" ] ; then
    mkdir -p "$DEPLOY/work"
fi

SITE_FILE="$DEPLOY/work/site.txt"

if [ "$SITE" ] || [ ! -e "$SITE_FILE" ] ; then
    echo ${SITE:-$SITE_DEFAULT} > "$SITE_FILE"
fi

PORT_FILE="$DEPLOY/work/port.txt"

if [ "$PORT" ] || [ ! -e "$PORT_FILE" ] ; then
    echo ${PORT:-$PORT_DEFAULT} > "$PORT_FILE"
fi

# Make a symbolic link to the secure site definition if the secure option was
# indicated. Any existing link is preserved.

if [ "$SECURE" ] ; then
    SITE_LINK="$DEPLOY/conf/${SITE:-$SITE_DEFAULT}.conf-ssl"

    if [ ! -e "$SITE_LINK" ] ; then
        ln -s "${SITE_PROFILE:-$SITE_PROFILE_DEFAULT}.conf-ssl" "$SITE_LINK"
    fi

# Make a symbolic link to any non-secure site definition otherwise. Any existing
# link is preserved.

else
    SITE_LINK="$DEPLOY/conf/${SITE:-$SITE_DEFAULT}.conf"

    if [ ! -e "$SITE_LINK" ] ; then
        ln -s "${SITE_PROFILE:-$SITE_PROFILE_DEFAULT}.conf" "$SITE_LINK"
    fi
fi

# vim: tabstop=4 expandtab shiftwidth=4
