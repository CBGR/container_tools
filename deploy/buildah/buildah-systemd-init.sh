#!/bin/sh

# SPDX-FileCopyrightText: The CentOS Project
# SPDX-License-Identifier: MIT

# See: https://hub.docker.com/_/centos
# See: https://stackoverflow.com/questions/36617368/docker-centos-7-with-systemctl-failed-to-mount-tmpfs-cgroup
# See: https://github.com/docker-library/docs/tree/master/centos

cd /lib/systemd/system/sysinit.target.wants/

for FILENAME in * ; do
    [ "$FILENAME" == systemd-tmpfiles-setup.service ] || rm -f "$FILENAME"
done

rm -f /lib/systemd/system/multi-user.target.wants/*
rm -f /etc/systemd/system/*.wants/*
rm -f /lib/systemd/system/local-fs.target.wants/*
rm -f /lib/systemd/system/sockets.target.wants/*udev*
rm -f /lib/systemd/system/sockets.target.wants/*initctl*
rm -f /lib/systemd/system/basic.target.wants/*
rm -f /lib/systemd/system/anaconda.target.wants/*

# vim: tabstop=4 expandtab shiftwidth=4
