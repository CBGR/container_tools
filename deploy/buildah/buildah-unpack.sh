#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/../..")
TOOLS="$BASEDIR/deploy/tools"

CONTAINER=$1
SOURCE=$2
TARGET=$3

MNT=$(buildah mount "$CONTAINER")

MNT_TARGET="$MNT/$TARGET"

if [ ! -e "$MNT_TARGET" ] ; then
    mkdir -p "$MNT_TARGET"
fi

"$TOOLS/tar_extract" "$SOURCE" "$MNT_TARGET"
buildah umount "$CONTAINER"

# vim: tabstop=4 expandtab shiftwidth=4
