#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/../..")

CONTAINER=$1
PARENT=$2
TARGET=$(realpath "$3")

shift 3

MNT=$(buildah mount "$CONTAINER")

# Enter the installed software location and archive the files.

"$BASEDIR/deploy/tools/archive.sh" "$MNT/$PARENT" "$TARGET" $*

buildah umount "$CONTAINER"

# vim: tabstop=4 expandtab shiftwidth=4
