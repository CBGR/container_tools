#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

PROGNAME=$(basename "$0")

CONTAINER=$1
PROGRAM=$2

if [ ! "$CONTAINER" ] || [ ! -e "$PROGRAM" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <container> <program>

Copy the program into the container and run it.
EOF
    exit 1
fi

# Make a temporary location for the program.

COPIED_PROGRAM="/tmp/"`basename "$PROGRAM"`

# Copy the program into the container.

buildah copy "$CONTAINER" "$PROGRAM" "$COPIED_PROGRAM"

# Run the copied program and then delete it.

buildah run "$CONTAINER" "$COPIED_PROGRAM"
buildah run "$CONTAINER" rm "$COPIED_PROGRAM"

# vim: tabstop=4 expandtab shiftwidth=4
