#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script makes a container deployment publicly accessible.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(realpath $(dirname "$0"))
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")

# Program parameters.

SITE=$1
PORT=$2

if [ "$SITE" = '--help' ] || [ ! "$SITE" ] || [ ! "$PORT" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <site> <port>

Make the indicated site available via the specified network port.

For example:

$PROGNAME testjaspar.uio.no 8080
EOF
    exit 1
fi

# Deploy the application using the proxy profile. This merely installs Web
# server configuration files outside the container environment on the host
# machine.

"$BASEDIR/deploy.sh" -p proxy -- -s "$SITE" -P "site-proxy" -p "$PORT"

# vim: tabstop=4 expandtab shiftwidth=4
