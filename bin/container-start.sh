#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to populate a container image with JASPAR data.

set -e

PROGNAME=$(basename "$0")

DEFAULT_PORT=8080

# Program parameters.

IMAGE=$1
PORT=${2:-"$DEFAULT_PORT"}

if [ ! "$IMAGE" ] || [ "$IMAGE" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <image> [ <port> ]

Start a container for the indicated image, mapping the specified network port
to port 80 inside the container. The default port to be used is $DEFAULT_PORT.

To expose a network port employed by the container, use the following:

sudo firewall-cmd --add-port=$DEFAULT_PORT/tcp

Here, $DEFAULT_PORT should be replaced with the chosen port, if appropriate.

Note that if the host Apache server has an appropriate site definition
proxying access to the indicated port, no firewall configuration is necessary.
EOF
    exit 1
fi

docker run -it -p "$PORT":80 -P localhost/"$IMAGE"

# vim: tabstop=4 expandtab shiftwidth=4
