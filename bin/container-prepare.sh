#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to populate a container image with all components of the
# given application.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(realpath $(dirname "$0"))
BASEDIR=$(realpath "$THISDIR/..")

# Program parameters.

HELP=
PACKAGES="/var/tmp/container-packages"

while getopts hp: NAME ; do
    case "$NAME" in
        h) HELP=y ;;
        p) PACKAGES=$OPTARG ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

PACKAGE=$1
CONTAINER=$2

if [ "$CONTAINER" ] && [ "$CONTAINER" != '--system' ] ; then
    if [ "$3" = '--image' ] ; then
        IMAGE=${4:-"$CONTAINER"}
        shift 2
    else
        IMAGE="$CONTAINER"
    fi
fi

# Require a package.

if [ "$HELP" ] || [ ! "$PACKAGE" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME [ -p <packages directory> ] <package name> \\
                 ( <container> [ --image <image> ] | --system )

Populate with the contents of the given package and its dependencies the
indicated build container or a build container having the same name as the
package, producing the indicated container image or an image having the
same name as the container.

Where --system is given, no container or image will be used, and the system
itself will be populated.

Use packages from the indicated or default package directory...

$PACKAGES

Such packages provide software used by the indicated package.

For example, to populate a container called website with the jaspar2020 package
and its dependencies:

$PROGNAME jaspar2020 website
EOF
    exit 1
fi

# Perform system-level preparations, if necessary.

if [ "$CONTAINER" = '--system' ] ; then
    "$BASEDIR/deploy.sh" -p system
fi

# Install the packages.

PACKAGE_MANIFEST="$PACKAGES/$PACKAGE.packages"

"$THISDIR/container-populate.sh" -p "$PACKAGES" -- "$PACKAGE_MANIFEST" "$CONTAINER"

# Stop here if preparing a system.

if [ "$CONTAINER" = '--system' ] ; then
    exit 0
fi

# Initialise services in the build container.

"$THISDIR/container-services.sh" "$CONTAINER"

# Make an image.

buildah commit "$CONTAINER" "$IMAGE"

# vim: tabstop=4 expandtab shiftwidth=4
