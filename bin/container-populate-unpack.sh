#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to populate a container image with all components of the
# application as given in a package manifest.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(realpath $(dirname "$0"))
BASEDIR=$(realpath "$THISDIR/..")

# Program parameters.

HELP=
PACKAGES=

while getopts hp: NAME ; do
    case "$NAME" in
        h) HELP=y ;;
        p) PACKAGES=$OPTARG ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

if [ "$1" = '--deploy' ] ; then
    DEPLOY=$1
    shift 1
else
    DEPLOY=
fi

CONTAINER=$1
PACKAGE_MANIFEST=$2

# Require a container option and package manifest.

if [ "$HELP" ] || [ ! "$CONTAINER" ] || [ ! -e "$PACKAGE_MANIFEST" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME [ -p <package directory> ] \\
                 ( [ --deploy ] <container> | --system ) <package manifest>

Populate the indicated build container using package files from the indicated or
default package directory.

For example, to populate a container called website with the jaspar2020 package
and accompanying software:

$PROGNAME website /var/tmp/container-packages/jaspar2020.packages
EOF
    exit 1
fi

# Conveniences based on the parameters.

unpack()
{
    if [ "$CONTAINER" = '--system' ] ; then
        "$BASEDIR/deploy/tools/unpack.sh" $*
    elif [ "$DEPLOY" ] ; then
        "$BASEDIR/deploy/tools/docker-unpack.sh" "$CONTAINER" $*
    else
        buildah unshare "$BASEDIR/deploy/buildah/buildah-unpack.sh" "$CONTAINER" $*
    fi
}

# Define the package directory.

if [ ! "$PACKAGES" ] ; then
    PACKAGES=$(dirname "$PACKAGE_MANIFEST")
fi

# Install all the separate software packages prepared for container use.

  grep -v '^$' "$PACKAGE_MANIFEST" \
| while read PACKAGE_NAME PACKAGE_LOCATION ; do

    # Investigate manifests provided for individual packages.

    MANIFEST="$PACKAGES/$PACKAGE_NAME.packages"

    if [ -e "$MANIFEST" ] && [ $(realpath "$MANIFEST") != $(realpath "$PACKAGE_MANIFEST") ] ; then
        "$0" -p "$PACKAGES" -- $DEPLOY "$CONTAINER" "$MANIFEST"
    else
        # Install the first filename variation of the package that is found.

        for PACKAGE_FILENAME in "$PACKAGES/$PACKAGE_NAME.tar"* ; do
            if [ -e "$PACKAGE_FILENAME" ] ; then
                unpack "$PACKAGE_FILENAME" "$PACKAGE_LOCATION"
                break
            else
                echo "Warning: $PACKAGE_NAME has no package file." 1>&2
            fi
        done
    fi
done

# vim: tabstop=4 expandtab shiftwidth=4
