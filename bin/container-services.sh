#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to initialise a build container with services able to
# support a Web application.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/..")

# Program parameters.

CONTAINER=$1

# Require a build container.

if [ ! "$CONTAINER" ] || [ "$CONTAINER" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <container>

Initialise services in the indicated build container.
EOF
    exit 1
fi

# Conveniences based on the parameters.

COPY="buildah copy $CONTAINER"
RUN="buildah run $CONTAINER"

# Install httpd and accompanying software.

# Although the which package is recorded as a requirement, the test of the
# system type uses the which command in order to determine which package
# management tools will be used, so the package is installed here as a
# bootstrapping measure.

$RUN dnf install -y httpd mod_ssl which

# Run an initialisation script to make systemd work. Then enable httpd.

"$BASEDIR/deploy/buildah/buildah-invoke.sh" "$CONTAINER" "$BASEDIR/deploy/buildah/buildah-systemd-init.sh"
$RUN systemctl enable httpd.service

# Set up container configuration.

buildah config --cmd "/usr/sbin/init" "$CONTAINER"
buildah config --port 80 "$CONTAINER"
buildah config --volume /sys/fs/cgroup "$CONTAINER"
buildah config --volume /run "$CONTAINER"

# vim: tabstop=4 expandtab shiftwidth=4
