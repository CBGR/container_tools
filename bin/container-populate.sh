#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to populate a container image with all components of the
# indicated application.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(realpath $(dirname "$0"))

# Program parameters.

HELP=
PACKAGES="/var/tmp/container-packages"

while getopts hp: NAME ; do
    case "$NAME" in
        h) HELP=y ;;
        p) PACKAGES=$OPTARG ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

PACKAGE_MANIFEST=$1

if [ "$2" = '--deploy' ] ; then
    DEPLOY=$2
    shift 1
else
    DEPLOY=
fi

CONTAINER=$2

# Require a package manifest and container option.

if [ "$HELP" ] || [ ! "$PACKAGE_MANIFEST" ] || [ ! "$CONTAINER" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME [ -p <packages directory> ] <package manifest> \\
                 ( [ --deploy ] <container> | --system )

Populate the indicated container for the given package manifest using package
files from the indicated or default package directory...

$PACKAGES

Such packages provide software used by the indicated package.

Where --deploy is indicated, a running container will be populated. Otherwise,
a build container will be populated.

For example, to populate a build container called public with the jaspar2020
package and accompanying software:

$PROGNAME jaspar2020.packages public

To populate a running container with some packages:

$PROGNAME jaspar2020.packages-deploy --deploy f49d9c297547
EOF
    exit 1
fi

# Test for packages.

"$THISDIR/container-populate-check.sh" -p "$PACKAGES" -- "$PACKAGE_MANIFEST"

# Install all the separate software packages prepared for container use.
# Propagate any flag controlling whether an image or a container is being
# populated.

"$THISDIR/container-populate-unpack.sh" -p "$PACKAGES" -- $DEPLOY "$CONTAINER" "$PACKAGE_MANIFEST"

# vim: tabstop=4 expandtab shiftwidth=4
