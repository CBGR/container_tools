#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to finalise a container image and introduce additional
# packages for deployment.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(realpath $(dirname "$0"))
BASEDIR=$(realpath "$THISDIR/..")

# Program parameters.

HELP=
PACKAGES="/var/tmp/container-packages"

while getopts hp: NAME ; do
    case "$NAME" in
        h) HELP=y ;;
        p) PACKAGES=$OPTARG ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

PACKAGE=$1
CONTAINER=$2
SITE=$3

if [ "$HELP" ] || [ ! "$PACKAGE" ] || [ ! "$CONTAINER" ] || [ ! "$SITE" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME [ -p <package directory> ] <package name> \\
                 ( <container> | --system ) <site> [ <option>... ]

Deploy resources within the indicated container or system for the specified
site.

For example (for a system-level deployment):

$PROGNAME jaspar2020 --system test-jaspar-elixir.uio.no

For example (for a container):

$PROGNAME jaspar2020 cfc7b8933e9c test-jaspar-elixir.uio.no

Use the following to see active containers:

docker container list

Remember to start the container before deploying resources.
EOF
    exit 1
fi

# Consume arguments so as to be able to pass the remaining ones on.

shift 3

# Install deployment-only packages such as data packages.

DEPLOY_PACKAGE_MANIFEST="$PACKAGES/$PACKAGE.packages-deploy"

"$THISDIR/container-populate.sh" -p "$PACKAGES" -- "$DEPLOY_PACKAGE_MANIFEST" --deploy "$CONTAINER"
"$THISDIR/container-deploy-postinst.sh" -p "$PACKAGES" -- "$CONTAINER" "$DEPLOY_PACKAGE_MANIFEST" "$SITE" $*

# Perform deployment actions for the packages.

PACKAGE_MANIFEST="$PACKAGES/$PACKAGE.packages"

"$THISDIR/container-deploy-postinst.sh" -p "$PACKAGES" -- "$CONTAINER" "$PACKAGE_MANIFEST" "$SITE" $*

# vim: tabstop=4 expandtab shiftwidth=4
