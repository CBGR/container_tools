#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to perform deployment actions on all packages in an
# application's package manifest.

set -e

PROGNAME=$(basename "$0")

# Program parameters.

HELP=
PACKAGES="/var/tmp/container-packages"

while getopts hp: NAME ; do
    case "$NAME" in
        h) HELP=y ;;
        p) PACKAGES=$OPTARG ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

CONTAINER=$1
PACKAGE_MANIFEST=$2

# Require a container option and package manifest.

if [ "$HELP" ] || [ ! "$CONTAINER" ] || [ ! -e "$PACKAGE_MANIFEST" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME [ -p <package directory> ] ( <container> | --system ) \\
                 <package manifest> [ <option>... ]

Perform deployment actions in the indicated deployment container or system using
the package manifest provided.

Additional options may be specified: these are passed to the deployment scripts
for the different packages.
EOF
    exit 1
fi

# Consume arguments so as to be able to pass the remaining ones on.

shift 2

# Conveniences based on the parameters.

deploy()
{
    PACKAGE_NAME=$1
    PACKAGE_LOCATION=$2
    shift 2

    # Use an alternative location for the deployment script where necessary.

    if [ "$PACKAGE_NAME" = $(basename "$PACKAGE_LOCATION") ] ; then
        DEPLOY="$PACKAGE_LOCATION/deploy.sh"
    else
        DEPLOY="$PACKAGE_LOCATION/var/tmp/$PACKAGE_NAME/deploy.sh"
    fi

    if [ "$CONTAINER" = '--system' ] ; then
        if [ -e "$DEPLOY" ] ; then
            "$DEPLOY" -o -p system -- -s $*
            break
        fi
    else
        if docker exec "$CONTAINER" sh -c "[ -e \"$DEPLOY\" ]" ; then
            docker exec "$CONTAINER" "$DEPLOY" -o -p docker -- -s $*
            break
        fi
    fi
}

# Install all the separate software packages prepared for container use.

  grep -v '^$' "$PACKAGE_MANIFEST" \
| while read PACKAGE_NAME PACKAGE_LOCATION ; do

    # Investigate manifests provided for individual packages.

    MANIFEST="$PACKAGES/$PACKAGE_NAME.packages"

    if [ -e "$MANIFEST" ] && [ $(realpath "$MANIFEST") != $(realpath "$PACKAGE_MANIFEST") ] ; then
        "$0" -p "$PACKAGES" -- "$CONTAINER" "$MANIFEST"
    else
        deploy "$PACKAGE_NAME" "$PACKAGE_LOCATION" $*
    fi
done

# vim: tabstop=4 expandtab shiftwidth=4
