#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to populate a container image with all components of the
# indicated application.

set -e

PROGNAME=$(basename "$0")

# Program parameters.

HELP=
PACKAGES=

while getopts hp: NAME ; do
    case "$NAME" in
        h) HELP=y ;;
        p) PACKAGES=$OPTARG ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

PACKAGE_MANIFEST=$1

# Require a package manifest.

if [ "$HELP" ] || [ ! "$PACKAGE_MANIFEST" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME [ -p <package directory> ] <package manifest>

Check the given package manifest.
EOF
    exit 1
fi

# Define the package directory.

if [ ! "$PACKAGES" ] ; then
    PACKAGES=$(dirname "$PACKAGE_MANIFEST")
fi

# Test for packages.

if [ ! -e "$PACKAGE_MANIFEST" ] ; then
    cat 1>&2 <<EOF
No package manifest found at:

$PACKAGE_MANIFEST
EOF
    exit 0
fi

  grep -v '^$' "$PACKAGE_MANIFEST" \
| while read PACKAGE_NAME PACKAGE_LOCATION ; do
    if [ ! "$PACKAGE_NAME" ] ; then
        continue
    fi

    # Investigate manifests provided for individual packages.

    MANIFEST="$PACKAGES/$PACKAGE_NAME.packages"

    if [ -e "$MANIFEST" ] && [ $(realpath "$MANIFEST") != $(realpath "$PACKAGE_MANIFEST") ] ; then
        "$0" -p "$PACKAGES" -- "$MANIFEST"
    else
        PACKAGE_FILE="$PACKAGES/$PACKAGE_NAME.tar"

        # Test for variations of the file.

        if [ ! -e "$PACKAGE_FILE"* ] ; then
            echo "Package file not found in $PACKAGES: $PACKAGE_FILE" 1>&2
            exit 1
        fi
    fi
done

# vim: tabstop=4 expandtab shiftwidth=4
